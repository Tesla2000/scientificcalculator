import pandas as pd
import numpy as np
import sympy as sy


def pozycja(liczba):
    miejsce = -1
    if 'e' in str(liczba):
        return int(str(liczba)[str(liczba).index('e') + 1:]) - 1
    if str(liczba)[0] != "0" and str(liczba).find(".") != -1:
        return str(liczba).find(".") - 2
    elif str(liczba)[0] != "0" and str(liczba).find(".") == -1:
        return len(str(liczba)) - 2
    while str(liczba)[0] == "0":
        liczba *= 10
        miejsce -= 1
    return miejsce


def round_up(liczba, miejsce):
    liczba = round(liczba, -pozycja(liczba) + 1)
    if liczba > round(liczba, -miejsce):
        liczba = round(liczba, -miejsce)
        liczba += 10 ** miejsce
    else:
        liczba = round(liczba, -miejsce)

    return liczba

def generuj_napis_special(elementy, wzor, wiersz, nazwa, wartosci, do_pracy):
    global special
    napis = str()
    ulamki = str()
    lista_pomocnicza = list()
    for element in elementy:
        lista_pomocnicza.append("Δ" + element)
    elementy2 = elementy + lista_pomocnicza
    for element in elementy:
        ulamki += (
                "\n + ("
                + chr(92)
                + "frac{\partial "
                + wzor
                + "}{\partial "
                + element
                + "}"
                + chr(92)
                + "cdot \Delta"
                + element
                + ")"
        )
    ulamki = ulamki.replace(" + ", "", 1)
    napis += ulamki.replace("**", "^")
    napis += " \n= "
    napis_pomocniczy = przekasztalc(wzor, elementy).replace(")**0.5", "")
    napis_pomocniczy = napis_pomocniczy.replace("(", "", 1)
    napis += napis_pomocniczy.replace("**", "^").replace('])^2', '])')
    napis += " = ("
    napis_pomocniczy = przekasztalc(wzor, elementy).replace('])**2', '])')
    elementy.sort(key=lambda k: len(k), reverse=True)
    for element in elementy2:
        if element in napis_pomocniczy and element in list(do_pracy.columns):
            start = 0
            for i in range(100):
                if wyrazenie_logiczne2(napis_pomocniczy, element, start):
                    napis_pomocniczy = napis_pomocniczy[:start] + napis_pomocniczy[
                                                                  start:
                                                                  ].replace(
                        element,
                        'str(do_pracy["' + element + '"][' + str(wiersz - 1) + ']) + "',
                        1,
                    )
                start = napis_pomocniczy.find(element, start) + 1
        if element in napis_pomocniczy and element in wartosci:
            start = 0
            for i in range(100):
                if wyrazenie_logiczne2(napis_pomocniczy, element, start):
                    napis_pomocniczy = napis_pomocniczy[:start] + napis_pomocniczy[
                                                                  start:
                                                                  ].replace(element,
                                                                            'str(wartosci["' + element + '"]) + "', 1)
                    start = napis_pomocniczy.find(element, start) + 1
    napis_pomocniczy = napis_pomocniczy.replace('Δstr(do_pracy["', 'str(do_pracy["Δ')
    napis_pomocniczy = napis_pomocniczy.replace('str(do_pracy["', '" + str(do_pracy["')
    napis_pomocniczy = napis_pomocniczy.replace('Δstr(wartosci["', 'str(wartosci["Δ')
    napis_pomocniczy = napis_pomocniczy.replace('str(wartosci["', '" + str(wartosci["')
    napis_pomocniczy = napis_pomocniczy.replace('" + ', "", 1)
    napis_pomocniczy = napis_pomocniczy.replace(")**0.5", '"))')
    napis_pomocniczy = napis_pomocniczy.replace("**", "^")
    napis_pomocniczy = napis_pomocniczy.replace("((", '(("', 1)
    napis_pomocniczy = napis_pomocniczy.replace("str", '" + str', 1)
    napis += eval(napis_pomocniczy)
    napis += " \n= "
    napis += str(do_pracy["Δ" + nazwa][wiersz - 1])
    return napis.replace("Δ", "\\" "Delta ").replace('*', '\\' 'cdot ')


def generuj_napis(elementy, wzor, wiersz, nazwa, wartosci, do_pracy):
    napis = (
            "Obliczono niepewność pomiarową "
            + nazwa
            + " typu B. "
            + "Dla wiersza "
            + str(wiersz)
            + "\n"
            + "u_B = "
    )
    napis += ""
    ulamki = str()
    lista_pomocnicza = list()
    for element in elementy:
        lista_pomocnicza.append("Δ" + element)
    elementy2 = elementy + lista_pomocnicza
    for element in elementy:
        ulamki += (
                "\n\sqrt{ + ("
                + chr(92)
                + "frac{\partial "
                + wzor
                + "}{\partial "
                + element
                + "}"
                + chr(92)
                + "cdot Δ"
                + element
                + ")^2}"
        )
    ulamki = ulamki.replace(" + ", "", 1)
    napis += ulamki.replace("**", "^")
    napis += " \n= \sqrt{"
    napis_pomocniczy = przekasztalc(wzor, elementy).replace(")**0.5", "}")
    napis_pomocniczy = napis_pomocniczy.replace("(", "", 1)
    napis += napis_pomocniczy.replace("**", "^")
    napis += " = \sqrt{("
    napis_pomocniczy = przekasztalc(wzor, elementy)
    elementy.sort(key=lambda k: len(k), reverse=True)
    for element in elementy2:
        if element in napis_pomocniczy and element in list(do_pracy.columns):
            start = 0
            for i in range(100):
                if wyrazenie_logiczne2(napis_pomocniczy, element, start):
                    napis_pomocniczy = napis_pomocniczy[:start] + napis_pomocniczy[
                                                                  start:
                                                                  ].replace(
                        element,
                        'str(do_pracy["' + element + '"][' + str(wiersz - 1) + ']) + "',
                        1,
                    )
                start = napis_pomocniczy.find(element, start) + 1
        if element in napis_pomocniczy and element in wartosci:
            start = 0
            for i in range(100):
                if wyrazenie_logiczne2(napis_pomocniczy, element, start):
                    napis_pomocniczy = napis_pomocniczy[:start] + napis_pomocniczy[
                                                                  start:
                                                                  ].replace(element,
                                                                            'str(wartosci["' + element + '"]) + "', 1)
                    start = napis_pomocniczy.find(element, start) + 1
    napis_pomocniczy = napis_pomocniczy.replace('Δstr(do_pracy["', 'str(do_pracy["Δ')
    napis_pomocniczy = napis_pomocniczy.replace('str(do_pracy["', '" + str(do_pracy["')
    napis_pomocniczy = napis_pomocniczy.replace('Δstr(wartosci["', 'str(wartosci["Δ')
    napis_pomocniczy = napis_pomocniczy.replace('str(wartosci["', '" + str(wartosci["')
    napis_pomocniczy = napis_pomocniczy.replace('" + ', "", 1)
    napis_pomocniczy = napis_pomocniczy.replace(")**0.5", '}"))')
    napis_pomocniczy = napis_pomocniczy.replace("**", "^")
    napis_pomocniczy = napis_pomocniczy.replace("((", '(("', 1)
    napis_pomocniczy = napis_pomocniczy.replace("str", '" + str', 1)
    napis += eval(napis_pomocniczy)
    napis += " \n= "
    napis += str(do_pracy["Δ" + nazwa][wiersz - 1])
    return napis.replace("Δ", "\\" "Delta ").replace('*', '\\' 'cdot ')


def przekasztalc(wzor, elementy, special=False):
    i = int()
    litera_poczatkowa = 192
    symbole = list()
    tlumacz = dict()
    wzor_koncowy = str()
    if not special:
        for element in elementy:
            while element in wzor:
                wzor = wzor.replace(element, chr(i + litera_poczatkowa))
            symbole.append(chr(i + litera_poczatkowa))
            tlumacz[chr(i + litera_poczatkowa)] = element
            i += 1
        i = int()
        for symbol in symbole:
            if symbol in wzor:
                symbol = sy.symbols(symbol)
                pochodna = sy.Derivative(wzor, symbol)
                pochodna = pochodna.doit()
                wzor_koncowy += (
                        " + (" + str(pochodna) + "*Δ" + chr(litera_poczatkowa + i) + ")**2"
                )
                i += 1
        wzor_koncowy = wzor_koncowy.replace(" + ", "(", 1)
        wzor_koncowy += ")**0.5"
        for zmiana in tlumacz:
            wzor_koncowy = wzor_koncowy.replace(zmiana, tlumacz[zmiana])
        return wzor_koncowy
    else:
        for element in elementy:
            while element in wzor:
                wzor = wzor.replace(element, chr(i + litera_poczatkowa))
            symbole.append(chr(i + litera_poczatkowa))
            tlumacz[chr(i + litera_poczatkowa)] = element
            i += 1
        i = int()
        for symbol in symbole:
            if symbol in wzor:
                symbol = sy.symbols(symbol)
                pochodna = sy.Derivative(wzor, symbol)
                pochodna = pochodna.doit()
                wzor_koncowy += (
                        " + ((" + str(pochodna) + "*Δ" + chr(litera_poczatkowa + i) + ")**2)**0.5"
                )
                i += 1
        wzor_koncowy = wzor_koncowy.replace(" + ", "(", 1)
        wzor_koncowy += ")"
        for zmiana in tlumacz:
            wzor_koncowy = wzor_koncowy.replace(zmiana, tlumacz[zmiana])
        print(wzor_koncowy.replace(')**2)**0.5', '|').replace('((', '|'))
        return wzor_koncowy


def miejsca_zaczace(liczba):
    liczba = abs(liczba)
    if liczba == 0:
        return liczba
    if (
            abs(round_up(liczba, pozycja(liczba)) - round_up(liczba, pozycja(liczba) + 1))
            / round_up(liczba, pozycja(liczba) + 1)
            < 0.1
    ):
        return round_up(liczba, pozycja(liczba) + 1)
    return round_up(liczba, pozycja(liczba))


def miejsca_znaczace_array(array):
    result = list()
    for element in array:
        element = miejsca_zaczace(element)
        result.append(element)
    return np.array(result)


def miejsca_znaczace_blad(do_pracy, kolumna):
    for i in range(len(do_pracy[kolumna])):
        if do_pracy[kolumna][i] != 0.0:
            do_pracy[kolumna][i] = round(
                do_pracy[kolumna][i], zaokraglenie(do_pracy["Δ" + kolumna][i])
            )
    return do_pracy[kolumna]


def zaokraglenie(liczba):
    if liczba == 0.0:
        return 100
    liczba = str(liczba)
    # if 'e' in str(liczba):
    #     return int(str(liczba)[str(liczba).index('e') + 1:]) - 1
    if liczba[0] != "0":
        liczba = liczba[: max(3, liczba.find(".") + 1)]
        return -liczba.count("0") + 1
    else:
        pozycja = 0
        while liczba[pozycja] == "0" or liczba[pozycja] == ".":
            pozycja += 1
        if len(liczba) - 1 == pozycja:
            return pozycja - 1
        elif liczba[pozycja + 1] != "0":
            return pozycja
        else:
            return pozycja - 1


def niepewnosc_b(wzor, z, special=False):
    y = z.copy()
    if not special:
        if wzor.find("if") == -1:
            wzor = eval(wzor)
            for wyraz in range(len(wzor)):
                wzor[wyraz] = miejsca_zaczace(wzor[wyraz] / (3 ** 0.5))
            return wzor
        else:
            wzor = wzor.replace("y", "y[i]")
            else_statement = wzor[wzor.find("else"):]
            start_statement = wzor[: wzor.find("if")]
            if_statement = wzor[wzor.find("if"): wzor.find("else")]
            for i in range(len(y)):
                y[i] = (
                           eval(start_statement + " " + if_statement + " " + else_statement)
                       ) / 3 ** 0.5
                y[i] = miejsca_zaczace(y[i])
            return y
    else:
        if wzor.find("if") == -1:
            wzor = eval(wzor)
            for wyraz in range(len(wzor)):
                wzor[wyraz] = miejsca_zaczace(wzor[wyraz])
            return wzor
        else:
            wzor = wzor.replace("y", "y[i]")
            else_statement = wzor[wzor.find("else"):]
            start_statement = wzor[: wzor.find("if")]
            if_statement = wzor[wzor.find("if"): wzor.find("else")]
            for i in range(len(y)):
                y[i] = (
                           eval(start_statement + " " + if_statement + " " + else_statement)
                       )
                y[i] = miejsca_zaczace(y[i])
            return y


def niepewnosc_b_wzgledna(wzor, z, special=False):
    y = z.copy()
    if not special:
        if wzor.find("if") == -1:
            wzor = eval(wzor)
            for wyraz in range(len(wzor)):
                wzor[wyraz] = miejsca_zaczace(wzor[wyraz] / y[wyraz] / (3 ** 0.5))
            return wzor * 100
        else:
            wzor = wzor.replace("y", "y[i]")
            else_statement = wzor[wzor.find("else"):]
            start_statement = wzor[: wzor.find("if")]
            if_statement = wzor[wzor.find("if"): wzor.find("else")]
            for i in range(len(y)):
                if z[i] != 0.0:
                    y[i] = (
                            (eval(start_statement + " " + if_statement + " " + else_statement))
                            / 3 ** 0.5
                            / z[i]
                    )
                    y[i] = miejsca_zaczace(y[i])
            return y * 100
    else:
        if wzor.find("if") == -1:

            wzor = eval(wzor)
            for wyraz in range(len(wzor)):
                wzor[wyraz] = miejsca_zaczace(wzor[wyraz] / y[wyraz])
            return wzor * 100
        else:
            wzor = wzor.replace("y", "y[i]")
            else_statement = wzor[wzor.find("else"):]
            start_statement = wzor[: wzor.find("if")]
            if_statement = wzor[wzor.find("if"): wzor.find("else")]
            for i in range(len(y)):
                if z[i] != 0.0:
                    y[i] = (
                            (eval(start_statement + " " + if_statement + " " + else_statement))
                            / z[i]
                    )
                    y[i] = miejsca_zaczace(y[i])
            return y * 100


def wyrazenie_logiczne(wzor1, element, start):
    if not wzor1.find(element, start) == 0:
        if (
                wzor1[wzor1.find(element, start) - 1].isalpha()
                or wzor1[wzor1.find(element, start) - 1] == '"'
        ) and wzor1[wzor1.find(element, start) - 1] != "Δ":
            return False
        else:
            if not wzor1.find(element, start) + len(element) == len(wzor1):
                if (
                        wzor1[wzor1.find(element, start) + len(element)].isalpha()
                        or wzor1[wzor1.find(element, start) + len(element)] == '"'
                        or wzor1[wzor1.find(element, start) + len(element)] == "."
                ):
                    return False
                else:
                    return True
            else:
                return True
    else:
        if not wzor1.find(element) + len(element) == len(wzor1):
            if (
                    wzor1[wzor1.find(element) + len(element)].isalpha()
                    or wzor1[wzor1.find(element, start) + len(element)] == "."
            ):
                return False
            else:
                return True
        else:
            return True


def wyrazenie_logiczne2(wzor1, element, start):
    if not wzor1.find(element, start) == 0:
        if (
                wzor1[wzor1.find(element, start) - 1].isalpha()
                or wzor1[wzor1.find(element, start) - 1] == '"'
        ):
            return False
        else:
            if not wzor1.find(element, start) + len(element) == len(wzor1):
                if (
                        wzor1[wzor1.find(element, start) + len(element)].isalpha()
                        or wzor1[wzor1.find(element, start) + len(element)] == '"'
                        or wzor1[wzor1.find(element, start) + len(element)] == "."
                ):
                    return False
                else:
                    return True
            else:
                return True
    else:
        if not wzor1.find(element) + len(element) == len(wzor1):
            if (
                    wzor1[wzor1.find(element) + len(element)].isalpha()
                    or wzor1[wzor1.find(element, start) + len(element)] == "."
                    or wzor1[wzor1.find(element, start) + len(element)] == '"'
            ):
                return False
            else:
                return True
        else:
            return True


def jednostka_na_procent(napis):
    pocz = napis.find("[")
    kon = napis.find("]")
    napis = napis.replace(napis[pocz + 1: kon], "%")
    return napis


def linia_trendu(x, y):
    wspolczynnik_kierunkowy = float()
    mianownik = float()
    for i in range(len(x)):
        mianownik += (x[i] - x.mean()) ** 2
    for i in range(len(x)):
        wspolczynnik_kierunkowy += ((x[i] - x.mean()) * (y[i] - y.mean())) / mianownik
    wyraz_wolny = miejsca_zaczace(y.mean() - wspolczynnik_kierunkowy * x.mean())
    linia_idealna = wspolczynnik_kierunkowy * x + wyraz_wolny
    blad_nieliniowosci = miejsca_zaczace(
        max(abs(y - linia_idealna)) / (x.max() - x.min())
    )
    histereza = list()
    for i in range(len(x)):
        x = list(x)
        a = x.count(x[i])
        mini_list = list()
        start = int()
        for j in range(a):
            mini_list.append(y[x.index(x[i], start)])
            start = x.index(x[i], start) + 1
        histereza.append(max(mini_list) - min(mini_list))
    histereza = miejsca_zaczace(max(histereza) / (max(y) - min(y)) * 100)
    r_kwadrat = sum(
        (wspolczynnik_kierunkowy * arg + wyraz_wolny - y.mean()) ** 2 for arg in x
    ) / sum((value - y.mean()) ** 2 for value in y)
    return locals()


def niepewnosc_b_show(wzor, wiersz, y):
    if wzor.find("if") == -1:
        wzor = eval(wzor)
        return wzor[wiersz]
    else:
        wzor = wzor.replace("y", "y[i]")
        else_statement = wzor[wzor.find("else"):]
        start_statement = wzor[: wzor.find("if")]
        if_statement = wzor[wzor.find("if"): wzor.find("else")]
        for i in range(len(y)):
            y[i] = eval(start_statement + " " + if_statement + " " + else_statement)
        return y[wiersz]


def tekst_edytowany(tekst):
    elementy = list()
    elementy.append(tekst[: (1 + tekst.find(":"))])
    tekst = tekst.replace(tekst[: (1 + tekst.find(":"))], "")
    elementy[-1] = elementy[-1].replace(":", "")
    elementy[-1] = elementy[-1].replace("y", "y[wiersz]")
    while ":" in tekst:
        elementy.append(
            tekst[
            : min(
                tekst.find("elif") if tekst.find("elif") > 0 else 1000,
                tekst.find("else") if tekst.find("else") > 0 else 1000,
                1 + tekst.find(":"),
            )
            ]
        )
        tekst = tekst.replace(
            tekst[
            : min(
                tekst.find("elif") if tekst.find("elif") > 0 else 1000,
                tekst.find("else") if tekst.find("else") > 0 else 1000,
                1 + tekst.find(":"),
            )
            ],
            "",
        )
        elementy[-1] = elementy[-1].replace(":", "")
        elementy[-1] = elementy[-1].replace("y", "y[wiersz]")
    elementy.append(tekst)
    return elementy


def niepewnosci(do_pracy, polecenia=dict(), special=False):
    global licznik_wiersz, licznik_wzor
    kolejnosc_column = list()
    for i in range(len(do_pracy.columns)):
        if "Unnamed" not in do_pracy.columns[i]:
            licznik_wzor += 1
            x = do_pracy.columns[i]
            y = do_pracy[x].copy()
            if not polecenia.get("wzor" + str(licznik_wzor), False):
                wzor = input(
                    "Wzór na niepewność aparaturową "
                    + str(x)
                    + " (należy wpisywać w postaci wielomianu gdzie y to wynik pomiaru): "
                )
            else:
                wzor = polecenia["wzor" + str(licznik_wzor)]
            wzor = wzor.replace("**", "^")
            wzor_py = wzor
            if wzor.find("y") == -1:
                wzor_py = wzor + "*y/y"
            wzor_py = wzor_py.replace("^", "**")
            wzor = wzor.replace("y", str(x))
            licznik_wiersz += 1
            if not polecenia.get("wiersz" + str(licznik_wiersz), False):
                wiersz = int(
                    input(
                        "Wprowadź wiersz, dla którego mają być wykonane przykładowe obliczenie (0 oznacza brak obliczeń): "
                    )
                )
            else:
                wiersz = polecenia["wiersz" + str(licznik_wiersz)]
            do_pracy["Δ" + str(x)] = niepewnosc_b(str(wzor_py), y, special)
            y = do_pracy[x].copy()
            if wiersz != 0 and not special:
                print(
                    "Obliczono niepewnosc pomiarową "
                    + str(x)
                    + " typu B."
                    + "Dla wiersza "
                    + str(wiersz)
                    + "\n"
                    + "u_B = "
                    + chr(92)
                    + "frac{u_p}{\sqrt3} = "
                    + chr(92)
                    + "frac{"
                    + wzor
                    + "}{\sqrt3} = "
                    + chr(92)
                    + "frac{"
                    + str(niepewnosc_b_show(str(wzor_py), wiersz - 1, y))
                    + "}{\sqrt3} = "
                    + str(niepewnosc_b_show(str(wzor_py), wiersz - 1, y) / 3 ** 0.5)
                    + "\nPo zaokrągleniu "
                    + str(niepewnosc_b(wzor_py, y)[wiersz - 1])
                )
            elif wiersz != 0 and special:
                print(
                    "Obliczono niepewnosc pomiarową "
                    + str(x)
                    + " metodą różniczki zupełnej."
                    + "Dla wiersza "
                    + str(wiersz)
                    + "\n"
                    + "\\Delta " + str(x) + " = "
                    + wzor
                    + " = "
                    + str(niepewnosc_b_show(str(wzor_py), wiersz - 1, y))
                    + " = "
                    + str(niepewnosc_b_show(str(wzor_py), wiersz - 1, y))
                    + "\nPo zaokrągleniu "
                    + str(niepewnosc_b(wzor_py, y, special)[wiersz - 1])
                )
            y = do_pracy[x].copy()
            do_pracy["δ" + jednostka_na_procent(str(x))] = niepewnosc_b_wzgledna(
                str(wzor_py), y, special
            )
            kolejnosc_column.extend(
                [x, "Δ" + str(x), "δ" + jednostka_na_procent(str(x))]
            )
    do_pracy = do_pracy[kolejnosc_column]
    for kolumna in list(do_pracy.columns):
        if "Δ" not in kolumna and "δ" not in kolumna and "Unnamed" not in kolumna:
            do_pracy[kolumna] = miejsca_znaczace_blad(do_pracy, kolumna)
    if not polecenia.get("dane do pliku", False):
        do_pracy.to_excel(input("Do jakiego pliku zapisać wyniki: "), index=False)
    else:
        do_pracy.to_excel(polecenia["dane do pliku"])


def dodatkowowe_kolumny(do_pracy, polecenia=dict(), special=False):
    global licznik_dodatkowych_elemetow, licznik_nazwy, licznik_wzor_dod, licznik_wiersz, licznik_wzor
    licznik_dodatkowych_elemetow += 1
    if not polecenia.get(
            "dodatkowy_element" + str(licznik_dodatkowych_elemetow), False
    ):
        kolejne_polecenie = input("Dodatkowe obliczenia (kolumna/wartość): ")
    else:
        kolejne_polecenie = polecenia[
            "dodatkowy_element" + str(licznik_dodatkowych_elemetow)
            ]
    wartosci = dict()
    elements = list()
    kolejnosc_column = list(do_pracy.columns)
    while kolejne_polecenie == "kolumna" or kolejne_polecenie == "wartość":
        licznik_dodatkowych_elemetow += 1
        licznik_nazwy += 1
        if not polecenia.get("nazwa" + str(licznik_nazwy), False):
            nazwa = input("Podaj nazwę: ")
        else:
            nazwa = polecenia["nazwa" + str(licznik_nazwy)]
        licznik_wzor_dod += 1
        if not polecenia.get("wzor_dod" + str(licznik_wzor_dod), False):
            wzor = input("Podaj wzór: ")
        else:
            wzor = polecenia["wzor_dod" + str(licznik_wzor_dod)]
        if kolejne_polecenie == "wartość":
            for wartosc in wartosci:
                if wartosc in wzor:
                    wzor = wzor.replace(wartosc, 'wartosci["' + wartosc + '"]')
            wartosc = eval(wzor)
            wartosci[nazwa] = wartosc
            if not polecenia.get("wzor" + str(licznik_wzor), False):
                wzor_na_blad = input(
                    "Podaj wzor na niepewność (gdzie y to wartość pomiaru): "
                )
            else:
                wzor_na_blad = polecenia["wzor" + str(licznik_wzor)]
            if "y" in wzor_na_blad:
                wzor_na_blad = wzor_na_blad.replace("y", "wartosc")
            wartosci["Δ" + nazwa] = miejsca_zaczace(eval(wzor_na_blad) / 3 ** 0.5)
            wartosci["δ" + jednostka_na_procent(nazwa)] = miejsca_zaczace(
                eval(wzor_na_blad) / 3 ** 0.5 / wartosc * 100
            )
        else:
            wzor1 = wzor
            for element in do_pracy.columns:
                if element in wzor1:
                    start = 0
                    for i in range(100):
                        if wyrazenie_logiczne(wzor1, element, start):
                            wzor1 = wzor1[:start] + wzor1[start:].replace(
                                element, 'do_pracy["' + element + '"]', 1
                            )
                        start = wzor1.find(element, start) + 1
            for wartosc in wartosci:
                if wartosc in wzor1:
                    start = 0
                    for i in range(100):
                        if wyrazenie_logiczne(wzor1, wartosc, start):
                            wzor1 = wzor1[:start] + wzor1[start:].replace(
                                wartosc, 'wartosci["' + wartosc + '"]', 1
                            )
                        start = wzor1.find(wartosc, start) + 1
            do_pracy[nazwa] = eval(wzor1)
            kolejnosc = sorted(
                list(do_pracy.columns[:-1]), key=lambda k: len(k), reverse=True
            )
            for element in kolejnosc:
                if element in wzor:
                    elements.append(element)
            for element in wartosci:
                if element in wzor:
                    elements.append(element)
            wzor_niepewnosc = przekasztalc(wzor, elements, special)
            kolejnosc = sorted(
                list(do_pracy.columns[:-1]), key=lambda k: len(k), reverse=True
            )
            for element in kolejnosc:
                if element in wzor_niepewnosc:
                    start = 0
                    for i in range(100):
                        if wyrazenie_logiczne(wzor_niepewnosc, element, start):
                            wzor_niepewnosc = wzor_niepewnosc[:start] + wzor_niepewnosc[
                                                                        start:
                                                                        ].replace(element,
                                                                                  'do_pracy["' + element + '"]', 1)
                        start = wzor_niepewnosc.find(element, start) + 1
                wzor_niepewnosc = wzor_niepewnosc.replace('Δdo_pracy["', 'do_pracy["Δ')
            for wartosc in wartosci:
                if wartosc in wzor_niepewnosc:
                    start = 0
                    for i in range(100):
                        if wyrazenie_logiczne(wzor_niepewnosc, wartosc, start):
                            wzor_niepewnosc = wzor_niepewnosc[:start] + wzor_niepewnosc[
                                                                        start:
                                                                        ].replace(wartosc,
                                                                                  'wartosci["' + wartosc + '"]', 1)
                        start = wzor_niepewnosc.find(wartosc, start) + 1
            wzor_niepewnosc = wzor_niepewnosc.replace('Δwartosci["', 'wartosci["Δ')
            do_pracy["Δ" + nazwa] = miejsca_znaczace_array(eval(wzor_niepewnosc))
            do_pracy["δ" + jednostka_na_procent(nazwa)] = miejsca_znaczace_array(
                eval(wzor_niepewnosc) / do_pracy[nazwa] * 100
            )
            kolejnosc_column.extend(
                [nazwa, "Δ" + nazwa, "δ" + jednostka_na_procent(nazwa)]
            )
            licznik_wiersz += 1
            if not polecenia.get("wiersz" + str(licznik_wiersz), False):
                wiersz = int(
                    input(
                        "Wprowadź wiersz, dla którego mają być wykonane przykładowe obliczenie (0 oznacza brak obliczeń): "
                    )
                )
            else:
                wiersz = polecenia["wiersz" + str(licznik_wiersz)]
            elements.sort(key=lambda k: len(k), reverse=True)
            if wiersz != 0 and not special:
                try:
                    print(
                        generuj_napis(elements, wzor, wiersz, nazwa, wartosci, do_pracy)
                    )
                except:
                    print("Nie udało się wygenerować napisu")
            elif wiersz != 0 and special:
                try:
                    print(
                        generuj_napis_special(elements, wzor, wiersz, nazwa, wartosci, do_pracy)
                    )
                except:
                    print("Nie udało się wygenerować napisu")
        do_pracy = do_pracy[kolejnosc_column]
        try:
            for kolumna in list(do_pracy.columns):
                if (
                        "Δ" not in kolumna
                        and "δ" not in kolumna
                        and "Unnamed" not in kolumna
                ):
                    do_pracy[kolumna] = miejsca_znaczace_blad(do_pracy, kolumna)
        except KeyError:
            print(
                "Nie udało się zaokrąglić do odpowiedniej postaci. By zaokrąglenie przebiegło prawidłowo należy we "
                "wzorze na wartości kolumny podać stałą w postaci liczby. Nie wiem dlaczego."
            )
        if kolejne_polecenie == "kolumna":
            if not polecenia.get("dane do pliku", False):
                do_pracy.to_excel(
                    input("Do jakiego pliku zapisać wyniki: "), index=False
                )
            else:
                do_pracy.to_excel(polecenia["dane do pliku"])
        if not polecenia.get(
                "dodatkowy_element" + str(licznik_dodatkowych_elemetow), False
        ):
            kolejne_polecenie = input("Dodatkowe obliczenia (kolumna/wartość): ")
        else:
            kolejne_polecenie = polecenia[
                "dodatkowy_element" + str(licznik_dodatkowych_elemetow)
                ]


def obliczenia_linia(do_pracy, polecenia=dict()):
    global licznik_linia
    if polecenia.get("linia" + str(licznik_linia), False):
        if not polecenia["linia" + str(licznik_linia)].get("x", False):
            x = input("oś x-ów (nazwa kolumny tabeli): ")
        else:
            x = polecenia["linia" + str(licznik_linia)]["x"]
        if not polecenia["linia" + str(licznik_linia)].get("y", False):
            while True:
                y = input(
                    "oś y-ów (nazwa kolumny tabeli), by przerwać dodawania wpisz pass: "
                )
                if y == "pass":
                    return None
                print(
                    "Histeraza: " + str(linia_trendu(do_pracy[x], do_pracy[y])["histereza"])
                )
                print(
                    "Współczynnik kierunkowy: "
                    + str(linia_trendu(do_pracy[x], do_pracy[y])["wspolczynnik_kierunkowy"])
                )
                print(
                    "Błąd nieliniowości: "
                    + str(linia_trendu(do_pracy[x], do_pracy[y])["blad_nieliniowosci"])
                )
                print(
                    "Błąd zera: "
                    + str(linia_trendu(do_pracy[x], do_pracy[y])["wyraz_wolny"])
                )
                print("R-kwadrat:", linia_trendu(do_pracy[x], do_pracy[y])["r_kwadrat"])
        else:
            y = polecenia["linia" + str(licznik_linia)]["y"]
            print("Histeraza: " + str(linia_trendu(do_pracy[x], do_pracy[y])["histereza"]))
            print(
                "Współczynnik kierunkowy: "
                + str(linia_trendu(do_pracy[x], do_pracy[y])["wspolczynnik_kierunkowy"])
            )
            print(
                "Błąd nieliniowości: "
                + str(linia_trendu(do_pracy[x], do_pracy[y])["blad_nieliniowosci"])
            )
            print(
                "Błąd zera: " + str(linia_trendu(do_pracy[x], do_pracy[y])["wyraz_wolny"])
            )
            print("R-kwadrat:", linia_trendu(do_pracy[x], do_pracy[y])["r_kwadrat"])
    else:
        x = input("oś x-ów (nazwa kolumny tabeli): ")
        while True:
            y = input(
                "oś y-ów (nazwa kolumny tabeli), by przerwać dodawania wpisz pass: "
            )
            if y == "pass":
                return None
            print(
                "Histeraza: " + str(linia_trendu(do_pracy[x], do_pracy[y])["histereza"])
            )
            print(
                "Współczynnik kierunkowy: "
                + str(linia_trendu(do_pracy[x], do_pracy[y])["wspolczynnik_kierunkowy"])
            )
            print(
                "Błąd nieliniowości: "
                + str(linia_trendu(do_pracy[x], do_pracy[y])["blad_nieliniowosci"])
            )
            print(
                "Błąd zera: "
                + str(linia_trendu(do_pracy[x], do_pracy[y])["wyraz_wolny"])
            )
            print("R-kwadrat:", linia_trendu(do_pracy[x], do_pracy[y])["r_kwadrat"])



polecenia = {'dane z pliku': 'etap2.xlsx', 'dane do pliku': 'etap3.xlsx', 'operacja1': 2,  'special': True, 'dodatkowy_element1': 'kolumna',
             'nazwa1': 'sprawnosc[%]', 'wzor_dod1': 'Pa[mW]/Pe[mW]*100'}


special = polecenia.get('special', False)
if not polecenia.get("dane z pliku", False):
    nazwa = input("Wprowadź nazwę pliku: ")
    if nazwa.endswith('.xlsx'):
        do_pracy = pd.read_excel(nazwa)
    elif nazwa.endswith('.csv'):
        do_pracy = pd.read_csv(nazwa)
    else:
        print('Plik powinien być w formacie xlsx lub csv.')
else:
    nazwa = polecenia["dane z pliku"]
    if nazwa.endswith('.xlsx'):
        do_pracy = pd.read_excel(nazwa)
    elif nazwa.endswith('.csv'):
        do_pracy = pd.read_csv(nazwa)
    else:
        print('Plik powinien być w formacie xlsx lub csv.')

licznik = licznik_linia = 1
licznik_wiersz = licznik_wzor = licznik_dodatkowych_elemetow = licznik_nazwy = licznik_wzor_dod = 0
while True:
    if not polecenia.get("operacja" + str(licznik), False):
        try:
            operacja = int(
                input(
                    "Jaką operację wykonać (1-obliczenia niepewności), (2-tworzenie dodatkowych kolumn), (3-obliczenia dotyczące linii trendu): "
                )
            )
        except ValueError:
            break
    else:
        operacja = polecenia["operacja" + str(licznik)]
    licznik += 1
    if operacja == 1:
        niepewnosci(do_pracy, polecenia, polecenia.get('special', False))
    elif operacja == 2:
        dodatkowowe_kolumny(do_pracy, polecenia, polecenia.get('special', False))
    elif operacja == 3:
        obliczenia_linia(do_pracy, polecenia)
    else:
        break

